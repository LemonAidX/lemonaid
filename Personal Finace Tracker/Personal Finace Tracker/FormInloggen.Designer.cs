﻿namespace Personal_Finace_Tracker
{
    partial class FormInloggen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInloggen));
            this.tb1 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.bInfo = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.bBackspace = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tb3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tb4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.tb5 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonAccount = new System.Windows.Forms.Button();
            this.gbaccount = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cbUsername = new System.Windows.Forms.ComboBox();
            this.llNewAccount = new System.Windows.Forms.LinkLabel();
            this.gbNewAccount = new System.Windows.Forms.GroupBox();
            this.dbi404113DataSet = new Personal_Finace_Tracker.dbi404113DataSet();
            this.loginBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.loginTableAdapter = new Personal_Finace_Tracker.dbi404113DataSetTableAdapters.LoginTableAdapter();
            this.bCancel = new System.Windows.Forms.Button();
            this.bRegister = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.gbaccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbNewAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dbi404113DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tb1
            // 
            this.tb1.BackColor = System.Drawing.Color.White;
            this.tb1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb1.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb1.Location = new System.Drawing.Point(12, 130);
            this.tb1.Name = "tb1";
            this.tb1.ReadOnly = true;
            this.tb1.Size = new System.Drawing.Size(83, 76);
            this.tb1.TabIndex = 6;
            this.tb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb1.UseSystemPasswordChar = true;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Silver;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(12, 202);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(83, 10);
            this.textBox1.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 220);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 80);
            this.button1.TabIndex = 8;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(161, 220);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(143, 82);
            this.button2.TabIndex = 9;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(310, 220);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(141, 82);
            this.button3.TabIndex = 10;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(12, 306);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(143, 82);
            this.button4.TabIndex = 11;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(161, 308);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(143, 82);
            this.button5.TabIndex = 12;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(310, 308);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(141, 82);
            this.button6.TabIndex = 13;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(12, 394);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(143, 82);
            this.button7.TabIndex = 14;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(161, 396);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(143, 82);
            this.button8.TabIndex = 15;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(310, 396);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(141, 82);
            this.button9.TabIndex = 16;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // bInfo
            // 
            this.bInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.bInfo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bInfo.BackgroundImage")));
            this.bInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bInfo.Location = new System.Drawing.Point(12, 482);
            this.bInfo.Name = "bInfo";
            this.bInfo.Size = new System.Drawing.Size(143, 82);
            this.bInfo.TabIndex = 17;
            this.bInfo.UseVisualStyleBackColor = false;
            this.bInfo.Click += new System.EventHandler(this.bInfo_Click);
            // 
            // button0
            // 
            this.button0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.button0.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button0.Location = new System.Drawing.Point(161, 484);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(143, 82);
            this.button0.TabIndex = 18;
            this.button0.Text = "0\r\n";
            this.button0.UseVisualStyleBackColor = false;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // bBackspace
            // 
            this.bBackspace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.bBackspace.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBackspace.BackgroundImage")));
            this.bBackspace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bBackspace.Location = new System.Drawing.Point(310, 484);
            this.bBackspace.Name = "bBackspace";
            this.bBackspace.Size = new System.Drawing.Size(141, 82);
            this.bBackspace.TabIndex = 19;
            this.bBackspace.UseVisualStyleBackColor = false;
            this.bBackspace.Click += new System.EventHandler(this.bBackspace_Click);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.Silver;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(101, 202);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(83, 10);
            this.textBox2.TabIndex = 21;
            this.textBox2.Visible = false;
            // 
            // tb2
            // 
            this.tb2.BackColor = System.Drawing.Color.White;
            this.tb2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb2.Location = new System.Drawing.Point(101, 130);
            this.tb2.Name = "tb2";
            this.tb2.ReadOnly = true;
            this.tb2.Size = new System.Drawing.Size(83, 76);
            this.tb2.TabIndex = 20;
            this.tb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb2.UseSystemPasswordChar = true;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.Silver;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(190, 202);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(83, 10);
            this.textBox3.TabIndex = 23;
            this.textBox3.Visible = false;
            // 
            // tb3
            // 
            this.tb3.BackColor = System.Drawing.Color.White;
            this.tb3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb3.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb3.Location = new System.Drawing.Point(190, 130);
            this.tb3.Name = "tb3";
            this.tb3.ReadOnly = true;
            this.tb3.Size = new System.Drawing.Size(83, 76);
            this.tb3.TabIndex = 22;
            this.tb3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb3.UseSystemPasswordChar = true;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.Silver;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(279, 202);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(83, 10);
            this.textBox4.TabIndex = 25;
            this.textBox4.Visible = false;
            // 
            // tb4
            // 
            this.tb4.BackColor = System.Drawing.Color.White;
            this.tb4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb4.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb4.Location = new System.Drawing.Point(279, 130);
            this.tb4.Name = "tb4";
            this.tb4.ReadOnly = true;
            this.tb4.Size = new System.Drawing.Size(83, 76);
            this.tb4.TabIndex = 24;
            this.tb4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb4.UseSystemPasswordChar = true;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.Silver;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Location = new System.Drawing.Point(368, 202);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(83, 10);
            this.textBox5.TabIndex = 27;
            this.textBox5.Visible = false;
            // 
            // tb5
            // 
            this.tb5.BackColor = System.Drawing.Color.White;
            this.tb5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb5.Font = new System.Drawing.Font("Microsoft Sans Serif", 40.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb5.Location = new System.Drawing.Point(368, 130);
            this.tb5.Name = "tb5";
            this.tb5.ReadOnly = true;
            this.tb5.Size = new System.Drawing.Size(83, 76);
            this.tb5.TabIndex = 26;
            this.tb5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb5.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(90, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(350, 44);
            this.label2.TabIndex = 31;
            this.label2.Text = "Selecteer gebruiker";
            // 
            // buttonAccount
            // 
            this.buttonAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.buttonAccount.Location = new System.Drawing.Point(344, 12);
            this.buttonAccount.Name = "buttonAccount";
            this.buttonAccount.Size = new System.Drawing.Size(107, 47);
            this.buttonAccount.TabIndex = 32;
            this.buttonAccount.Text = "Wijzig account";
            this.buttonAccount.UseVisualStyleBackColor = false;
            this.buttonAccount.Click += new System.EventHandler(this.button13_Click);
            // 
            // gbaccount
            // 
            this.gbaccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.gbaccount.Controls.Add(this.llNewAccount);
            this.gbaccount.Controls.Add(this.cbUsername);
            this.gbaccount.Location = new System.Drawing.Point(12, 12);
            this.gbaccount.Name = "gbaccount";
            this.gbaccount.Size = new System.Drawing.Size(439, 202);
            this.gbaccount.TabIndex = 34;
            this.gbaccount.TabStop = false;
            this.gbaccount.Text = "Account keuze";
            this.gbaccount.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(20, 80);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // cbUsername
            // 
            this.cbUsername.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.loginBindingSource, "Username", true));
            this.cbUsername.DataSource = this.loginBindingSource;
            this.cbUsername.DisplayMember = "Username";
            this.cbUsername.FormattingEnabled = true;
            this.cbUsername.Location = new System.Drawing.Point(6, 115);
            this.cbUsername.Name = "cbUsername";
            this.cbUsername.Size = new System.Drawing.Size(427, 24);
            this.cbUsername.TabIndex = 0;
            this.cbUsername.ValueMember = "Username";
            this.cbUsername.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // llNewAccount
            // 
            this.llNewAccount.AutoSize = true;
            this.llNewAccount.Location = new System.Drawing.Point(158, 68);
            this.llNewAccount.Name = "llNewAccount";
            this.llNewAccount.Size = new System.Drawing.Size(132, 17);
            this.llNewAccount.TabIndex = 1;
            this.llNewAccount.TabStop = true;
            this.llNewAccount.Text = "Nog geen account?";
            this.llNewAccount.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llNewAccount_LinkClicked);
            // 
            // gbNewAccount
            // 
            this.gbNewAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(111)))));
            this.gbNewAccount.Controls.Add(this.tbPassword);
            this.gbNewAccount.Controls.Add(this.textBox7);
            this.gbNewAccount.Controls.Add(this.tbUsername);
            this.gbNewAccount.Controls.Add(this.label3);
            this.gbNewAccount.Controls.Add(this.label1);
            this.gbNewAccount.Controls.Add(this.bRegister);
            this.gbNewAccount.Controls.Add(this.bCancel);
            this.gbNewAccount.Location = new System.Drawing.Point(12, 12);
            this.gbNewAccount.Name = "gbNewAccount";
            this.gbNewAccount.Size = new System.Drawing.Size(439, 202);
            this.gbNewAccount.TabIndex = 36;
            this.gbNewAccount.TabStop = false;
            this.gbNewAccount.Text = "Account registratie";
            this.gbNewAccount.Visible = false;
            // 
            // dbi404113DataSet
            // 
            this.dbi404113DataSet.DataSetName = "dbi404113DataSet";
            this.dbi404113DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // loginBindingSource
            // 
            this.loginBindingSource.DataMember = "Login";
            this.loginBindingSource.DataSource = this.dbi404113DataSet;
            // 
            // loginTableAdapter
            // 
            this.loginTableAdapter.ClearBeforeFill = true;
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(203, 121);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(99, 33);
            this.bCancel.TabIndex = 0;
            this.bCancel.Text = "Annuleren";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bRegister
            // 
            this.bRegister.Location = new System.Drawing.Point(332, 121);
            this.bRegister.Name = "bRegister";
            this.bRegister.Size = new System.Drawing.Size(99, 33);
            this.bRegister.TabIndex = 1;
            this.bRegister.Text = "Registreren";
            this.bRegister.UseVisualStyleBackColor = true;
            this.bRegister.Click += new System.EventHandler(this.bRegister_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "Gebruikersnaam";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 29);
            this.label3.TabIndex = 3;
            this.label3.Text = "Wachtwoord";
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(203, 61);
            this.tbUsername.MaxLength = 50;
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(230, 22);
            this.tbUsername.TabIndex = 4;
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(11, 121);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(186, 63);
            this.textBox7.TabIndex = 5;
            this.textBox7.Text = "*Bij wachtwoord wordt verwacht dat u een vijf cijferige code invoert.";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(203, 93);
            this.tbPassword.MaxLength = 5;
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(230, 22);
            this.tbPassword.TabIndex = 6;
            this.tbPassword.UseSystemPasswordChar = true;
            this.tbPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbWachtwoord_KeyPress);
            // 
            // FormInloggen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(460, 579);
            this.Controls.Add(this.gbNewAccount);
            this.Controls.Add(this.gbaccount);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.bInfo);
            this.Controls.Add(this.buttonAccount);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.tb5);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.bBackspace);
            this.Controls.Add(this.tb4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.tb3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.tb2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tb1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormInloggen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inloggen";
            this.Load += new System.EventHandler(this.FormInloggen_Load);
            this.gbaccount.ResumeLayout(false);
            this.gbaccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbNewAccount.ResumeLayout(false);
            this.gbNewAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dbi404113DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loginBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button bInfo;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button bBackspace;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox tb2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox tb3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox tb4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox tb5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonAccount;
        private System.Windows.Forms.GroupBox gbaccount;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel llNewAccount;
        private System.Windows.Forms.ComboBox cbUsername;
        private System.Windows.Forms.GroupBox gbNewAccount;
        private dbi404113DataSet dbi404113DataSet;
        private System.Windows.Forms.BindingSource loginBindingSource;
        private dbi404113DataSetTableAdapters.LoginTableAdapter loginTableAdapter;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bRegister;
        private System.Windows.Forms.Button bCancel;
    }
}

