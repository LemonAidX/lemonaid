﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Personal_Finace_Tracker
{
    class Database
    {
        SqlConnection sqlcon;
        public Database()
        {
            sqlcon = new SqlConnection(@"Data Source=mssql.fhict.local;Initial Catalog=dbi404113;Persist Security Info=True;User ID=dbi404113;Password=LemonAID");
        }
        
        public bool GetLogin(string username, string password)
        {            
            string query = "SELECT * FROM [Login] WHERE Username = @Username AND Password = @Password";

            SqlCommand cmd = new SqlCommand(query, sqlcon);

            cmd.Parameters.AddWithValue("@Username", username);
            cmd.Parameters.AddWithValue("@Password", password);

            sqlcon.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    return true;
                }
                return false;
            }                                      
        }

        public void Accountmaken(string username, string password)
        {
            sqlcon.Open();

            string query = "INSERT INTO [Login] (Username, Password) VALUES (@Username, @Password)";

            SqlCommand cmd = new SqlCommand(query, sqlcon);

            cmd.Parameters.AddWithValue("@Username", username);
            cmd.Parameters.AddWithValue("@Password", password);

            cmd.ExecuteNonQuery();

            this.sqlcon.Close();
        }
    }   
}
