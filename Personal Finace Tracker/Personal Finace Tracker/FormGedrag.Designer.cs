﻿namespace Personal_Finace_Tracker
{
    partial class FormGedrag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbWantToSpend = new System.Windows.Forms.GroupBox();
            this.gbAreSpending = new System.Windows.Forms.GroupBox();
            this.lbWatWilJeUitGeven = new System.Windows.Forms.Label();
            this.lbWatGeefJeUit = new System.Windows.Forms.Label();
            this.gbWantToSpend.SuspendLayout();
            this.gbAreSpending.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbWantToSpend
            // 
            this.gbWantToSpend.Controls.Add(this.lbWatWilJeUitGeven);
            this.gbWantToSpend.Location = new System.Drawing.Point(110, 150);
            this.gbWantToSpend.Name = "gbWantToSpend";
            this.gbWantToSpend.Size = new System.Drawing.Size(200, 100);
            this.gbWantToSpend.TabIndex = 0;
            this.gbWantToSpend.TabStop = false;
            // 
            // gbAreSpending
            // 
            this.gbAreSpending.Controls.Add(this.lbWatGeefJeUit);
            this.gbAreSpending.Location = new System.Drawing.Point(473, 150);
            this.gbAreSpending.Name = "gbAreSpending";
            this.gbAreSpending.Size = new System.Drawing.Size(200, 100);
            this.gbAreSpending.TabIndex = 1;
            this.gbAreSpending.TabStop = false;
            // 
            // lbWatWilJeUitGeven
            // 
            this.lbWatWilJeUitGeven.AutoSize = true;
            this.lbWatWilJeUitGeven.Location = new System.Drawing.Point(7, 22);
            this.lbWatWilJeUitGeven.Name = "lbWatWilJeUitGeven";
            this.lbWatWilJeUitGeven.Size = new System.Drawing.Size(179, 17);
            this.lbWatWilJeUitGeven.TabIndex = 0;
            this.lbWatWilJeUitGeven.Text = "Per maand wil je x uitgeven";
            // 
            // lbWatGeefJeUit
            // 
            this.lbWatGeefJeUit.AutoSize = true;
            this.lbWatGeefJeUit.Location = new System.Drawing.Point(7, 21);
            this.lbWatGeefJeUit.Name = "lbWatGeefJeUit";
            this.lbWatGeefJeUit.Size = new System.Drawing.Size(157, 17);
            this.lbWatGeefJeUit.TabIndex = 0;
            this.lbWatGeefJeUit.Text = "Per maand geef je x uit.";
            // 
            // FormGedrag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gbAreSpending);
            this.Controls.Add(this.gbWantToSpend);
            this.Name = "FormGedrag";
            this.Text = "FormGedrag";
            this.gbWantToSpend.ResumeLayout(false);
            this.gbWantToSpend.PerformLayout();
            this.gbAreSpending.ResumeLayout(false);
            this.gbAreSpending.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbWantToSpend;
        private System.Windows.Forms.GroupBox gbAreSpending;
        private System.Windows.Forms.Label lbWatWilJeUitGeven;
        private System.Windows.Forms.Label lbWatGeefJeUit;
    }
}