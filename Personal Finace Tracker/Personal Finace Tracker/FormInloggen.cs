﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Personal_Finace_Tracker
{
    public partial class FormInloggen : Form
    {
        private string gebruikersnaam = ""; // Field!

        public FormInloggen()
        {
            InitializeComponent();
        }

        //Methodes
        private void nummer(int getal)
        {
            Database db = new Database();
            int i = 0;
            if (tb1.Text == "")
            {
                tb1.Text = getal.ToString();
                tb1.BackColor = Color.Silver;
                textBox2.Visible = true;
                i++;
            }
            else if (tb1.Text != "" && tb2.Text == "")
            {
                tb2.Text = getal.ToString();
                tb2.BackColor = Color.Silver;
                textBox3.Visible = true;
                i += 2;
            }
            else if (tb1.Text != "" && tb2.Text != "" && tb3.Text == "")
            {
                tb3.Text = getal.ToString();
                tb3.BackColor = Color.Silver;
                textBox4.Visible = true;
                i += 3;
            }
            else if (tb1.Text != "" && tb2.Text != "" && tb3.Text != "" && tb4.Text == "")
            {
                tb4.Text = getal.ToString();
                tb4.BackColor = Color.Silver;
                textBox5.Visible = true;
                i += 4;
            }
            else
            {
                tb5.Text = getal.ToString();
                tb5.BackColor = Color.Silver;
                i += 5;
                string username = gebruikersnaam;
                string password = $"{tb1.Text}{tb2.Text}{tb3.Text}{tb4.Text}{tb5.Text}";

                if (db.GetLogin(username, password))
                {
                    FormHoofdmenu secondform = new FormHoofdmenu();
                    secondform.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Wrong password, please try again.");
                    tb1.Text = "";
                    tb1.BackColor = Color.White;
                    tb2.Text = "";
                    tb2.BackColor = Color.White;
                    textBox2.Visible = false;
                    tb3.Text = "";
                    tb3.BackColor = Color.White;
                    textBox3.Visible = false;
                    tb4.Text = "";
                    tb4.BackColor = Color.White;
                    textBox4.Visible = false;
                    tb5.Text = "";
                    tb5.BackColor = Color.White;
                    textBox5.Visible = false;
                }
            }
        }

        private void account(string naam)
        {
            label2.Text = naam.ToString();
            gbaccount.Visible = false;
            tb1.Text = "";
            tb1.BackColor = Color.White;
            tb2.Text = "";
            tb2.BackColor = Color.White;
            textBox2.Visible = false;
            tb3.Text = "";
            tb3.BackColor = Color.White;
            textBox3.Visible = false;
            tb4.Text = "";
            tb4.BackColor = Color.White;
            textBox4.Visible = false;
            tb5.Text = "";
            tb5.BackColor = Color.White;
            textBox5.Visible = false;

            gebruikersnaam = naam;
        }

        private void bBackspace_Click(object sender, EventArgs e)
        {
            int i = 0;
            if (tb5.Text != "" && tb4.Text != "" && tb3.Text != "" && tb2.Text != "" && tb1.Text != "")
            {
                tb5.Text = "";
                tb5.BackColor = Color.White;
                textBox5.Visible = true;
                i = 4;
            }
            else if (tb4.Text != "" && tb3.Text != "" && tb2.Text != "" && tb1.Text != "")
            {
                tb4.Text = "";
                tb4.BackColor = Color.White;
                textBox4.Visible = true;
                textBox5.Visible = false;
                i = 3;
            }
            else if (tb3.Text != "" && tb2.Text != "" && tb1.Text != "")
            {
                tb3.Text = "";
                tb3.BackColor = Color.White;
                textBox3.Visible = true;
                textBox4.Visible = false;
                i = 2;
            }
            else if (tb2.Text != "" && tb1.Text != "")
            {
                tb2.Text = "";
                tb2.BackColor = Color.White;
                textBox2.Visible = true;
                textBox3.Visible = false;
                i = 1;
            }
            else
            {
                tb1.Text = "";
                tb1.BackColor = Color.White;
                textBox2.Visible = false;
                i = 0;
            }
        }

        //Overig
        private void button1_Click(object sender, EventArgs e)
        {
            nummer(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            nummer(2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            nummer(3);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            nummer(4);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            nummer(5);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            nummer(6);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            nummer(7);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            nummer(8);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            nummer(9);
        }

        private void button0_Click(object sender, EventArgs e)
        {
            nummer(0);
        }

        private void bInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This app is made by LemonAID©. Copyright included!");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            gbaccount.Visible = true;
            gbaccount.Visible = true;
            button0.Enabled = false;
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;
            button7.Enabled = false;
            button8.Enabled = false;
            button9.Enabled = false;
            bInfo.Enabled = false;
            bBackspace.Enabled = false;
        }

        private void llNewAccount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            gbaccount.Visible = false;
            gbNewAccount.Visible = true;
        }

        private void FormInloggen_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dbi404113DataSet.Login' table. You can move, or remove it, as needed.
            this.loginTableAdapter.Fill(this.dbi404113DataSet.Login);

        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            gbaccount.Visible = true;
            gbNewAccount.Visible = false;
        }

        private void tbWachtwoord_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }


            if (e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
        }

        private void bRegister_Click(object sender, EventArgs e)
        {
            Database db = new Database();
            string username = tbUsername.Text;
            string password = tbPassword.Text;

            db.Accountmaken(username, password);

            MessageBox.Show("Account succesvol geregistreerd als: " + username + ".", "account registratie", MessageBoxButtons.OK, MessageBoxIcon.Information);

            string connstr = @"Data Source=mssql.fhict.local;Initial Catalog=dbi404113;Persist Security Info=True;User ID=dbi404113;Password=LemonAID";
            string query = "SELECT [Username] FROM [Login]";
            SqlDataAdapter adapter = new SqlDataAdapter(query, connstr);
            DataTable table = new DataTable();
            adapter.Fill(table);
            loginBindingSource = new BindingSource();
            loginBindingSource.DataSource = table;
            cbUsername.DataSource = loginBindingSource;

            gbaccount.Visible = false;
            gbNewAccount.Visible = false;
            button0.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;
            bInfo.Enabled = true;
            bBackspace.Enabled = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            account(cbUsername.Text);
            button0.Enabled = true;
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;
            bInfo.Enabled = true;
            bBackspace.Enabled = true;
        }
    }
}
