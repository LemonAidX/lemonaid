﻿namespace Personal_Finace_Tracker
{
    partial class FormSpaardoelen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.pcSparen = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lbPiechart = new System.Windows.Forms.Label();
            this.lbSavingtarget = new System.Windows.Forms.Label();
            this.lbTargetvalue = new System.Windows.Forms.Label();
            this.lbSavings = new System.Windows.Forms.Label();
            this.bTerug = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pcSparen)).BeginInit();
            this.SuspendLayout();
            // 
            // pcSparen
            // 
            chartArea1.Name = "ChartArea1";
            this.pcSparen.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.pcSparen.Legends.Add(legend1);
            this.pcSparen.Location = new System.Drawing.Point(474, 87);
            this.pcSparen.Name = "pcSparen";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.pcSparen.Series.Add(series1);
            this.pcSparen.Size = new System.Drawing.Size(259, 240);
            this.pcSparen.TabIndex = 0;
            this.pcSparen.Text = "chart1";
            // 
            // lbPiechart
            // 
            this.lbPiechart.AutoSize = true;
            this.lbPiechart.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPiechart.Location = new System.Drawing.Point(471, 67);
            this.lbPiechart.Name = "lbPiechart";
            this.lbPiechart.Size = new System.Drawing.Size(260, 29);
            this.lbPiechart.TabIndex = 1;
            this.lbPiechart.Text = "Pie chart about savings";
            // 
            // lbSavingtarget
            // 
            this.lbSavingtarget.AutoSize = true;
            this.lbSavingtarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSavingtarget.Location = new System.Drawing.Point(65, 142);
            this.lbSavingtarget.Name = "lbSavingtarget";
            this.lbSavingtarget.Size = new System.Drawing.Size(161, 32);
            this.lbSavingtarget.TabIndex = 2;
            this.lbSavingtarget.Text = "Spaardoel: ";
            // 
            // lbTargetvalue
            // 
            this.lbTargetvalue.AutoSize = true;
            this.lbTargetvalue.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTargetvalue.Location = new System.Drawing.Point(65, 206);
            this.lbTargetvalue.Name = "lbTargetvalue";
            this.lbTargetvalue.Size = new System.Drawing.Size(185, 32);
            this.lbTargetvalue.TabIndex = 3;
            this.lbTargetvalue.Text = "Doel bedrag: ";
            // 
            // lbSavings
            // 
            this.lbSavings.AutoSize = true;
            this.lbSavings.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSavings.Location = new System.Drawing.Point(65, 275);
            this.lbSavings.Name = "lbSavings";
            this.lbSavings.Size = new System.Drawing.Size(175, 32);
            this.lbSavings.TabIndex = 4;
            this.lbSavings.Text = "Spaar saldo:";
            // 
            // bTerug
            // 
            this.bTerug.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bTerug.Location = new System.Drawing.Point(44, 22);
            this.bTerug.Name = "bTerug";
            this.bTerug.Size = new System.Drawing.Size(83, 38);
            this.bTerug.TabIndex = 5;
            this.bTerug.Text = "Terug";
            this.bTerug.UseVisualStyleBackColor = true;
            // 
            // FormSpaardoelen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bTerug);
            this.Controls.Add(this.lbSavings);
            this.Controls.Add(this.lbTargetvalue);
            this.Controls.Add(this.lbSavingtarget);
            this.Controls.Add(this.lbPiechart);
            this.Controls.Add(this.pcSparen);
            this.Name = "FormSpaardoelen";
            this.Text = "FormSpaardoelen";
            ((System.ComponentModel.ISupportInitialize)(this.pcSparen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataVisualization.Charting.Chart pcSparen;
        private System.Windows.Forms.Label lbPiechart;
        private System.Windows.Forms.Label lbSavingtarget;
        private System.Windows.Forms.Label lbTargetvalue;
        private System.Windows.Forms.Label lbSavings;
        private System.Windows.Forms.Button bTerug;
    }
}