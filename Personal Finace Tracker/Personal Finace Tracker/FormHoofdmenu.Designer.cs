﻿namespace Personal_Finace_Tracker
{
    partial class FormHoofdmenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHoofdmenu));
            this.bUitloggen = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bRekeningen = new System.Windows.Forms.Button();
            this.bGedrag = new System.Windows.Forms.Button();
            this.bSpaargegevens = new System.Windows.Forms.Button();
            this.bSpaardoelen = new System.Windows.Forms.Button();
            this.bInstellingen = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bUitloggen
            // 
            this.bUitloggen.Location = new System.Drawing.Point(734, 21);
            this.bUitloggen.Name = "bUitloggen";
            this.bUitloggen.Size = new System.Drawing.Size(176, 103);
            this.bUitloggen.TabIndex = 0;
            this.bUitloggen.Text = "Uitloggen";
            this.bUitloggen.UseVisualStyleBackColor = true;
            this.bUitloggen.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bUitloggen);
            this.groupBox1.Controls.Add(this.bRekeningen);
            this.groupBox1.Controls.Add(this.bGedrag);
            this.groupBox1.Controls.Add(this.bSpaargegevens);
            this.groupBox1.Controls.Add(this.bSpaardoelen);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(916, 130);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // bRekeningen
            // 
            this.bRekeningen.Enabled = false;
            this.bRekeningen.Location = new System.Drawing.Point(6, 21);
            this.bRekeningen.Name = "bRekeningen";
            this.bRekeningen.Size = new System.Drawing.Size(176, 103);
            this.bRekeningen.TabIndex = 4;
            this.bRekeningen.Text = "Rekeningen";
            this.bRekeningen.UseVisualStyleBackColor = true;
            this.bRekeningen.Click += new System.EventHandler(this.button5_Click);
            // 
            // bGedrag
            // 
            this.bGedrag.Location = new System.Drawing.Point(552, 21);
            this.bGedrag.Name = "bGedrag";
            this.bGedrag.Size = new System.Drawing.Size(176, 103);
            this.bGedrag.TabIndex = 2;
            this.bGedrag.Text = "Gedrag";
            this.bGedrag.UseVisualStyleBackColor = true;
            // 
            // bSpaargegevens
            // 
            this.bSpaargegevens.Location = new System.Drawing.Point(370, 21);
            this.bSpaargegevens.Name = "bSpaargegevens";
            this.bSpaargegevens.Size = new System.Drawing.Size(176, 103);
            this.bSpaargegevens.TabIndex = 3;
            this.bSpaargegevens.Text = "Spaargegevens";
            this.bSpaargegevens.UseVisualStyleBackColor = true;
            // 
            // bSpaardoelen
            // 
            this.bSpaardoelen.Location = new System.Drawing.Point(188, 21);
            this.bSpaardoelen.Name = "bSpaardoelen";
            this.bSpaardoelen.Size = new System.Drawing.Size(176, 103);
            this.bSpaardoelen.TabIndex = 1;
            this.bSpaardoelen.Text = "Spaardoelen";
            this.bSpaardoelen.UseVisualStyleBackColor = true;
            // 
            // bInstellingen
            // 
            this.bInstellingen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bInstellingen.BackgroundImage")));
            this.bInstellingen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bInstellingen.Location = new System.Drawing.Point(786, 287);
            this.bInstellingen.Name = "bInstellingen";
            this.bInstellingen.Size = new System.Drawing.Size(52, 51);
            this.bInstellingen.TabIndex = 5;
            this.bInstellingen.UseVisualStyleBackColor = true;
            // 
            // FormHoofdmenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 458);
            this.Controls.Add(this.bInstellingen);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormHoofdmenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bUitloggen;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bRekeningen;
        private System.Windows.Forms.Button bGedrag;
        private System.Windows.Forms.Button bSpaardoelen;
        private System.Windows.Forms.Button bInstellingen;
        private System.Windows.Forms.Button bSpaargegevens;
    }
}